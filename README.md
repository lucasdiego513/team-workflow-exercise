# Exercise - Team Workflow

This project presents an exercise to practice team workflow as part of the NavalSubWeb course.

Dando continuidade aos estudos das aulas anteriores, o desafio de hoje consiste em exercitar o fluxo de trabalho durante o desenvolvimento em equipe. Para realizar o trabalho, vocês precisam formar **2 equipes de 4 pessoas**. O objetivo de cada equipe é implementar o consumo e tratamento dos dados recebidos de uma API pública de teste [SWAPI - The Star Wars API](https://swapi.dev/), usando o protocolo HTTP e o padrão REST.

Para realizar o desafio, as seguintes etapas devem ser seguidas:

<ul>
    <li><u><b>ETAPA 1:</b></u> Cada equipe deverá fazer o fork (“cópia”) do repositório. Todos os membros da equipe deverão trabalhar com esse novo repositório copiado.</li>
    <li><u><b>ETAPA 2:</b></u> Cada integrante da equipe deverá adotar um título:
        <ul>
        <li><b>DEV 1:</b> Fulano</li>
        <li><b>DEV 2:</b> Cicrano</li>
        <li><b>DEV 3:</b> Beltrano</li>
        <li><b>DEV 4:</b> Trajano</li>
        </ul>
    </li>
    <li><u><b>ETAPA 3:</b></u> Uma vez que o DEV já clonou o repositório para sua máquina local, deverá resolver suas tasks específicas, por exemplo:
        <div align="center">
            Exemplo para a implementação dos parsers.
            <p>
                <img src="media/example.png" />
            </p>
        </div>
    </li>
</ul>

<u><b>OBSERVAÇÕES:</b></u>

1. <u><b>A arquitetura criada não deverá ser alterada!</b></u>
2. **Cada DEV deverá criar uma nova branch, a partir da _develop_, para fazer suas tasks.**
3. **No mínimo um commit para cada task deverá ser realizado.**
4. **Não delete sua branch após dar o merge na develop.**
