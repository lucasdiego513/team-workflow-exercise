import axios from "axios";

// STAR WARS API CONSUMPTION

// TASK: Consume star wars API to get planets data. - TODO: **DEV 1**
async function getPlanets() {}

// TASK: Consume star wars API to get people data. - TODO: **DEV 2**
async function getPeople() {}

// TASK: Consume star wars API to get films data. - TODO: **DEV 3**
async function getFilms() {}

// TASK: Consume star wars API to get species data. - TODO: **DEV 4**
async function getSpecies() {}

// TASK: Consume star wars API to get vehicles data. - TODO: **DEV 1**
async function getVehicles() {}

// TASK: Consume star wars API to get starships data. - TODO: **DEV 2**
async function getStarships() {}

// TASK: Consume star wars API to get a single planet data. - TODO: **DEV 3**
async function getSinglePlanet(id) {}

// TASK: Consume star wars API to get a single person data. - TODO: **DEV 4**
async function getSinglePerson(id) {}

// TASK: Consume star wars API to get a single film data. - TODO: **DEV 1**
async function getSingleFilm(id) {}

// TASK:  Consume star wars API to get a single species data. - TODO: **DEV 2**
async function getSingleSpecies(id) {}

// TASK: Consume star wars API to get a single vehicle data. - TODO: **DEV 3**
async function getSingleVehicle(id) {}

// TASK: Consume star wars API to get a single starship data. - TODO: **DEV 4**
async function getSingleStarship(id) {}

const starWarsConsumer = {
  getPlanets,
  getPeople,
  getFilms,
  getSpecies,
  getVehicles,
  getStarships,
  getSinglePlanet,
  getSinglePerson,
  getSingleFilm,
  getSingleSpecies,
  getSingleVehicle,
  getSingleStarship,
};

export default starWarsConsumer;
