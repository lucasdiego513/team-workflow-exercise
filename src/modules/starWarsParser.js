// LIST OF PARSER FUNCTIONS TO GET SPECIFIC DATA FROM THE STAR WARS API CONSUMER.

// TASK: Parse the star wars planets data to get the name of each planet. TODO: **DEV 1**
function getPlanetsNames(planets) {}

// TASK: Parse the star wars planets data to get the rotation period of each planet. TODO: **DEV 2**
function getPlanetsRotationPeriod(planets) {}

// TASK: Parse the star wars single planet data to get its terrain. TODO: **DEV 3**
function getSinglePlanetTerrain(planet) {}

// TASK: Parse the star wars people data to get the name of each person. TODO: **DEV 4**
function getPeopleNames(people) {}

// TASK: Parse the star wars people data to get the height of each person. TODO: **DEV 1**
function getPeopleHeights(people) {}

// TASK: Parse the star wars single person data to get its hair color. TODO: **DEV 2**
function getSinglePersonHairColor(person) {}

// TASK: Parse the star wars films data to get the title of each film. TODO: **DEV 3**
function getFilmsTitles(films) {}

// TASK: Parse the star wars single film data to get its opening crawl. TODO: **DEV 4**
function getSingleFilmOpeningCrawl(film) {}

// TASK: Parse the star wars species data to get the name of each species. TODO: **DEV 1**
function getSpeciesNames(species) {}

// TASK: Parse the star wars single species data to get its language. TODO: **DEV 2**
function getSingleSpeciesLanguage(specie) {}

// TASK: Parse the star wars vehicles data to get the name of each vehicle. TODO: **DEV 3**
function getVehiclesNames(vehicles) {}

// TASK: Parse the star wars single vehicle data to get its model. TODO: **DEV 4**
function getSingleVehicleModel(vehicle) {}

// TASK: Parse the star wars starships data to get the name of each starship. TODO: **DEV 1**
function getStarshipsNames(starships) {}

// TASK: Parse the star wars single starship data to get its manufacturer. TODO: **DEV 2**
function getSingleStarshipManufacturer(starship) {}

const starWarsParser = {
  getPlanetsNames,
  getPlanetsRotationPeriod,
  getSinglePlanetTerrain,
  getPeopleNames,
  getPeopleHeights,
  getSinglePersonHairColor,
  getFilmsTitles,
  getSingleFilmOpeningCrawl,
  getSpeciesNames,
  getSingleSpeciesLanguage,
  getVehiclesNames,
  getSingleVehicleModel,
  getStarshipsNames,
  getSingleStarshipManufacturer,
};

export default starWarsParser;
