import starWarsConsumer from "../consumer/starWarsConsumer.js";
import starWarsParser from "./starWarsParser.js";

// STAR WARS VIEW

// IMPORTANT: IMPLEMENT A CATCH BLOCK FOR EVERY CONSUMER FUNCTION

// TASK: Get planets data from starWarsConsumer and parse them to get the name of each planet with starWarsParser, then show the result in the console. TODO: **DEV 1**
function viewPlanetsName() {}

// TASK: Get planets data from starWarsConsumer and parse them to get the rotation period of each planet with starWarsParser, then show the result in the console. TODO: **DEV 2**
function viewPlanetsRotationPeriod() {}

// TASK: Get single planet data from starWarsConsumer and parse it to get its terrain with starWarsParser, then show the result in the console. TODO: **DEV 3**
function viewSinglePlanetTerrain(planetId) {}

// TASK: Get people data from starWarsConsumer and parse them to get the name of each person with starWarsParser, then show the result in the console. TODO: **DEV 4**
function viewPeopleNames() {}

// TASK: Get people data from starWarsConsumer and parse them to get the height of each person with starWarsParser, then show the result in the console. TODO: **DEV 1**
function viewPeopleHeights() {}

// TASK: Get single person data from starWarsConsumer and parse it to get its hair color with starWarsParser, then show the result in the console. TODO: **DEV 2**
function viewSinglePersonHairColor(personId) {}

// TASK: Get films data from starWarsConsumer and parse them to get the title of each film with starWarsParser, then show the result in the console. TODO: **DEV 3**
function viewFilmsTitles() {}

// TASK: Get single film data from starWarsConsumer and parse it to get its opening crawl with starWarsParser, then show the result in the console. TODO: **DEV 4**
function viewSingleFilmOpeningCrawl(filmId) {}

// TASK: Get species data from starWarsConsumer and parse them to get the name of each species with starWarsParser, then show the result in the console. TODO: **DEV 1**
function viewSpeciesNames() {}

// TASK: Get single species data from starWarsConsumer and parse it to get its language with starWarsParser, then show the result in the console. TODO: **DEV 2**

function viewSingleSpeciesLanguage(speciesId) {}

// TASK: Get vehicles data from starWarsConsumer and parse them to get the name of each vehicle with starWarsParser, then show the result in the console. TODO: **DEV 3**
function viewVehiclesNames() {}

// TASK: Get single vehicle data from starWarsConsumer and parse it to get its model with starWarsParser, then show the result in the console. TODO: **DEV 4**
function viewSingleVehicleModel(vehicleId) {}

// TASK: Get starships data from starWarsConsumer and parse them to get the name of each starship with starWarsParser, then show the result in the console. TODO: **DEV 1**
function viewStarshipsNames() {}

// TASK: Get single starship data from starWarsConsumer and parse it to get its manufacturer with starWarsParser, then show the result in the console. TODO: **DEV 2**
function viewSingleStarshipManufacturer(starshipId) {}

const starWarsView = {
  viewPlanetsName,
  viewPlanetsRotationPeriod,
  viewSinglePlanetTerrain,
  viewPeopleNames,
  viewPeopleHeights,
  viewSinglePersonHairColor,
  viewFilmsTitles,
  viewSingleFilmOpeningCrawl,
  viewSpeciesNames,
  viewSingleSpeciesLanguage,
  viewVehiclesNames,
  viewSingleVehicleModel,
  viewStarshipsNames,
  viewSingleStarshipManufacturer,
};

export default starWarsView;
